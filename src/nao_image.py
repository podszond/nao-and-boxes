from naoqi import ALProxy
import numpy as np
import cv2
import sys


class NaoImage:
    def __init__(self, ip, port):
        # create proxy to communicate with nao
        # ip is 10.10.48.252
        # port is 9559
        try:
            self.proxy = ALProxy('ALVideoDevice', ip, port)
        except Exception as e:
            print(e)
            sys.exit(1)

        # name of module
        self.module_name = 'cam'

        self.proxy.unsubscribeAllInstances(self.module_name)

        # index of the camera in the video system
        # top camera 0
        # bottom camera 1
        camera_id = 0

        # resolution
        # 0 is 160*120px
        # 1 is 320*240px
        # 2 is 640*480px
        # 3 is 1280*960px
        resolution = 2
        # bgr colorspace
        # buffer contains triplet on the format 0xrrggbb equivalent to
        # 3 unsigned chars
        color_space = 13
        # frames per second
        fps = 30

        self.camera = self.proxy.subscribeCamera(
                self.module_name,
                camera_id,
                resolution,
                color_space,
                fps
                )

    def get_image(self):
        # retrieves the latest image from video source
        image_container = self.proxy.getImageRemote(self.camera)

        # create new 1-d array initialized from image_container data
        # image_container[6] is array of size height * width * nblayer
        # of image data dtype is data-type it must be exact
        try:
            ndarray = np.fromstring(
                    image_container[6],
                    dtype=np.uint8
                    )
        except ValueError as err:
            print(type(err), err.args)
            return

        # reshape the array according to resolution
        image = ndarray.reshape((480, 640, 3))

        return image

    def __del__(self):
        self.proxy.unsubscribe(self.module_name)
