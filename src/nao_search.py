from naoqi import ALProxy
import sys
from helpers import get_theta
from nao_image import NaoImage


class NaoSearch:
    def __init__(self, ip, port):
        self.nao_image = NaoImage(ip, port)
        self.state = 0
        self.angle_change = 0.6
        self.head_move_speed = 0.12
        try:
            self.motion_proxy  = ALProxy('ALMotion', ip, port)
        except Exception as e:
            print('could not create proxy to ALMotion')
            print(e)
            sys.exit(1)

    # return true if found
    def find_and_rotate(self):
        if get_theta(self.nao_image.get_image()):
            return True
        # look to left 
        self.rotate_head_left()
        if get_theta(self.nao_image.get_image()):
            self.final_robot_rotate()
            return True
        # look to right
        self.rotate_head_return_middle()
        self.rotate_head_right()
        if get_theta(self.nao_image.get_image()):
            self.final_robot_rotate()
            return True
        # look further right
        self.rotate_head_right()
        if get_theta(self.nao_image.get_image()):
            self.final_robot_rotate()
            return True
        # look further left
        self.rotate_head_return_middle()
        self.rotate_head_left()
        self.rotate_head_left()
        if get_theta(self.nao_image.get_image()):
            self.final_robot_rotate()
            return True
        self.rotateHeadReturnMiddle()
        return False
    
    def rotate_head_left(self):
        self.state = self.state - 1
        self.update_head_pos()
        return
    
    def rotate_head_return_middle(self):
        self.state = 0
        self.update_head_pos()
        return
    
    def rotate_head_right(self):
        self.state = self.state + 1
        self.update_head_pos()
        return
    
    def update_head_pos(self):
        self.motion_proxy.angleInterpolationWithSpeed(
                "HeadYaw",
                self.state * self.angle_change,
                self.head_move_speed
                )
    
    def final_robot_rotate(self):
        self.motion_proxy.moveTo(0, 0, self.state * self.angle_change)
        self.rotate_head_return_middle()
        return
