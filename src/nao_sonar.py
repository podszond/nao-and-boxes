from naoqi import ALProxy
import sys


class NaoSonar:
    def __init__(self, ip, port):
        try:
            self.memory_proxy = ALProxy('ALMemory', ip, port)
        except Exception as e:
            print('could not create proxy to ALMemory')
            print(e)
            sys.exit(1)

        try:
            self.sonar_proxy = ALProxy('ALSonar', ip, port)
        except Exception as e:
            print('could not create proxy to ALSonar')
            print(e)
            sys.exit(1)

        # subscribe to sonars
        # this will launch sonars (at hardware level)
        # and start data acquisition
        self.sonar_proxy.subscribe('nao-and-boxes')

    def get_distance(self):
        # get sonar max distance
        # distance in meters to the first obstacle
        left = 'Device/SubDeviceList/US/Left/Sensor/Value'
        right = 'Device/SubDeviceList/US/Right/Sensor/Value'
        return max(
                self.memory_proxy.getData(left),
                self.memory_proxy.getData(right)
                )

    def __del__(self):
        # unsubscribe from sonars
        # this will stop sonars (at hardware level)
        self.sonar_proxy.unsubscribe('nao-and-boxes')
