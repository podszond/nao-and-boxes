import cv2
from frame_transformation import find_contour, draw_contour_rectangle
import sys

def compute_center_delta(frame_width, x, w):
    # find center coordination of rectangle
    return (frame_width / 2) - (x + w / 2)

def compute_rotation(img_width, x, w):
    delta = compute_center_delta(img_width, x, w)
    # TODO comment
    # compute rotation of robot in radians
    return 0.532063622 * ((float(delta)) / (float(img_width) / 2.0))

def get_theta(frame):
    if frame is None:
        print('frame is None')
        sys.exit(1)

    x, y, w, h = find_contour(frame)

    # display the resulting frame
    draw_contour_rectangle(frame, x, y, w, h)
    cv2.imshow('frame', frame)
    if cv2.waitKey(10) & 0xFF == ord('q'):
	sys.exit(0)

    # in case no contour find continue
    if x == 0 or y == 0 or w == 0 or h == 0:
        print('object not found')
        return None

    return compute_rotation(frame.shape[1], x, w)
