import cv2
from naoqi import ALProxy
from nao_image import NaoImage
from nao_sonar import NaoSonar
import frame_transformation as ft
from nao_search import NaoSearch
import sys
from helpers import get_theta


class NaoNavigation:
    def __init__(self, ip, port):
        self.nao_image = NaoImage(ip, port)
        self.nao_sonar = NaoSonar(ip, port)
        self.nao_search = NaoSearch(ip, port)

        try:
            self.motion_proxy  = ALProxy('ALMotion', ip, port)
        except Exception as e:
            print('could not create proxy to ALMotion')
            print(e)
            sys.exit(1)

        try:
            self.posture_proxy = ALProxy('ALRobotPosture', ip, port)
        except Exception as e:
            print('could not create proxy to ALRobotPosture')
            print(e)
            sys.exit(1)

        # wake up robot
        self.motion_proxy.wakeUp()

        # send robot to pose init
        self.posture_proxy.goToPosture('StandInit', 0.5)

    def rotate(self):
        while(True):
            theta = get_theta(self.nao_image.get_image())
            if theta is None:
                continue

            threshold = 0.1
            if abs(theta) < threshold:
                print('in range', 'theta:', theta, 'threshold', threshold)
                return
            else: 
                self.motion_proxy.moveTo(0, 0, theta)
                print('not in range', 'theta:', theta, 'threshold', threshold)

    def navigate(self):
        self.long_navigation()
        print('complete')
        self.short_navigation()

    def short_navigation(self):
        while True:
            self.motion_proxy.moveToward(0.1, 0, 0)
            frame = self.nao_image.get_image()
            x, y, w, h = ft.find_contour(frame)
            kps = ft.find_corners(frame)

            if kps is None:
                print('corners not found')
                continue

            a = ft.find_closest_keypoint(x + 0, y + 0, kps)
            b = ft.find_closest_keypoint(x + 0, y + h, kps)
            c = ft.find_closest_keypoint(x + w, y + 0, kps)
            d = ft.find_closest_keypoint(x + w, y + h, kps)

            frame = ft.draw_keypoints(frame, [a, b, c, d])
            ft.draw_contour_rectangle(frame, x, y, w, h)

            cv2.imshow('frame', frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

            left_delta = abs(a.pt[1] - b.pt[1])
            right_delta = abs(c.pt[1] - d.pt[1])

            print('left delta' + str(left_delta) +
                    'right delta' + str(right_delta))

            delta = left_delta - right_delta

            if abs(delta) > 2:
                print('delta abs(' + str(delta) + ') > 1')
                self.motion_proxy.moveToward(0, 0, 0)

                if left_delta < right_delta:
                    self.motion_proxy.moveTo(0, -0.1, 0)
                else:
                    self.motion_proxy.moveTo(0, 0.1, 0)

                self.rotate()

                self.motion_proxy.moveToward(0.1, 0, 0)

            min_distance = 70
            if left_delta < min_distance and right_delta < min_distance:
                print('delta' + str(delta))
                break

        self.motion_proxy.moveToward(0, 0, 0)
        print('short navigation complete')

    def long_navigation(self):
        speed = 0.5
        # turn into right direction
        if self.nao_search.find_and_rotate() is False:
            print('object not found')
            return False
        else:
            print('find complete')

        self.rotate()
        print('rotate complete')
        
        while True:
            self.motion_proxy.moveToward(speed, 0, 0)
            # check object position
            theta = get_theta(self.nao_image.get_image())
            if theta is None:
                continue

            threshold = 0.1
            if abs(theta) > threshold:
                print('out range', 'theta:', theta, 'threshold', threshold)
                # stop robot
                self.motion_proxy.moveToward(0, 0, 0)
                # rotate
                self.rotate()
                # continue walking
                self.motion_proxy.moveToward(speed, 0, 0)

            # check distance
            distance = self.nao_sonar.get_distance()
            print('sonar distance is', distance)
            #
            # http://doc.aldebaran.com/1-14/family/robots/sonar_robot.html
            # detection range: 0.25m - 2.55m
            #
            sonar_threshold = 0.5
            if distance < sonar_threshold:
                print('distance:', distance, 'treshold:', sonar_threshold)
                break

        # stop forward
        self.motion_proxy.moveToward(0, 0, 0)
        return True

    def __del__(self):
        # destroy all windows
        cv2.destroyAllWindows()

if __name__ == '__main__':
    nao_navigation = NaoNavigation('10.10.48.252', 9559)
    nao_navigation.navigate()
