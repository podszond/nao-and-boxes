import numpy as np
import cv2


def frame_transformation(frame):
    # convert brg to hvs
    hvs = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # threshold the hvs image to get only red colors
    # for lower red color
    lower_red = cv2.inRange(
            hvs,
            np.array([0, 100, 100]),
            np.array([5, 255, 255])
            )
    # for upper red color
    upper_red = cv2.inRange(
            hvs,
            np.array([160, 100, 100]),
            np.array([179, 255, 255])
            )

    # put upper and lower red together
    mask = cv2.bitwise_or(lower_red, upper_red)

    # mask is binary image
    return mask

def find_contour(image):
    frame = frame_transformation(image)

    contours, hierarchy = cv2.findContours(
            frame,
            cv2.RETR_TREE,
            cv2.CHAIN_APPROX_SIMPLE
            )

    if len(contours) == 0:
        return 0, 0, 0, 0

    # find contour with biggest area
    contour = max(contours, key=cv2.contourArea)

    # treshold on countour area
    image_area = image.shape[0] * image.shape[1]
    if cv2.contourArea(contour) < image_area * 0.001:
        return 0, 0, 0, 0

    # bound rectangle to the contour
    x, y, w, h = cv2.boundingRect(contour)

    return x, y, w, h

def draw_contour_rectangle(image, x, y, w, h):
    bgr_green = (0, 255, 0)
    cv2.rectangle(image, (x, y), (x + w, y + h), bgr_green)

def find_corners(image):
    """
    returns array of KeyPoints
    """
    # initiate star detector
    orb = cv2.ORB()
    # find the keypoints with orb
    kp = orb.detect(image, None)
    # compute the descriptors with orb
    kp, des = orb.compute(image, kp)

    return kp

def draw_keypoints(image, kps):
    return cv2.drawKeypoints(image, kps, color=(0, 255, 0), flags=0)

def sqrt_euclidian_distace(x1, x2, y1, y2):
    return (x1 - y1) ** 2 + (x2 - y2) ** 2

def find_closest_keypoint(x, y, kps):
    min_val = sqrt_euclidian_distace(x, y, kps[0].pt[0], kps[0].pt[1])
    min_kp = kps[0]
    for kp in kps:
        val = sqrt_euclidian_distace(x, y, kp.pt[0], kp.pt[1])
        if val < min_val:
            min_val = val
            min_kp = kp

    return min_kp

# for testing
if __name__ == '__main__':
    cap = cv2.VideoCapture(0)

    while(True):
        # capture frame by frame
        ret, frame = cap.read()

        if ret is False:
            print('cap.read returned False')
            break

        # our operations on the frame come here
        x, y, w, h = find_contour(frame)
        kps = find_corners(frame)
        a = find_closest_keypoint(x + 0, y + 0, kps)
        b = find_closest_keypoint(x + 0, y + h, kps)
        c = find_closest_keypoint(x + w, y + 0, kps)
        d = find_closest_keypoint(x + w, y + h, kps)

        print(abs(a.pt[1] - b.pt[1]), abs(c.pt[1] - d.pt[1]))

        frame = draw_keypoints(frame, [a, b, c, d])
        draw_contour_rectangle(frame, x, y, w, h)

        # display the resulting frame
        cv2.imshow('frame', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # when everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()
